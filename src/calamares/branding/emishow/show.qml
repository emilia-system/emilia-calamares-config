import QtQuick 2.0;
import calamares.slideshow 1.0;

Presentation
{
    id: presentation

    function nextSlide() {
        console.log("QML Component (default slideshow) Next slide");
        presentation.goToNextSlide();
    }

    
    Timer {
        id: advanceTimer
        interval: 10000
        running: presentation.activatedInCalamares
        repeat: true
        onTriggered: nextSlide()
    }

    Slide {
        Image {
            id: thanks
            source: "emiliamain.png"
            width: 800; height: 480
            fillMode: Image.PreserveAspectFit
            anchors.centerIn: parent
        }
    }
    
    Slide {
        Image {
            id: packages
            source: "packages.png"
            width: 800; height: 480
            fillMode: Image.PreserveAspectFit
            anchors.centerIn: parent
        }
    }

    Slide {
        Image {
            id: desktops
            source: "desktops.png"
            width: 800; height: 480
            fillMode: Image.PreserveAspectFit
            anchors.centerIn: parent
        }
    }
    
    function onActivate() {
        console.log("QML Component (default slideshow) activated");
        presentation.currentSlide = 0;
    }
    
     function onLeave() {
        console.log("QML Component (default slideshow) deactivated");
    }
}
